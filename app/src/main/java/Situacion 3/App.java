/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package situacion;

public class App {
    public static void main(String[] args) 
    {
        Cliente cliente = new Cliente();
        cliente.setNombre("Gustavo");
        cliente.setApellido("Contreras");
        cliente.setDNI(43995185);
        cliente.setDomicilio("Barrio 144 viviendas Santa Rosa 110");
        cliente.setNumeroTelefono(371142);

        cliente.vehiculo.setMarca("Ford");
        cliente.vehiculo.setModelo("Fiesta");
        cliente.vehiculo.setPatente("UWU 404");
        cliente.vehiculo.setKilometraje(100000);
        cliente.vehiculo.setMotivoReparacion("Chirrido al frenar");
        cliente.vehiculo.setMotivoReparacion("Revision de cubierta");
        cliente.vehiculo.setMotivoReparacion("Perdida de aceite");

        Taller taller = new Taller();

        taller.efectuarFicha(cliente);
    }
}
