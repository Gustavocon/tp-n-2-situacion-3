package situacion;
public class Cliente
{
    // Atributos
    private String nombre;
    private String apellido;
    private Integer dni;
    private String domicilio;
    private Integer numeroTelefono;

    Vehiculo vehiculo = new Vehiculo();

    // Metodos constructores
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
    public void setApellido(String apellido)
    {
        this.apellido = apellido;
    }
    public void setDNI(Integer dni)
    {
        this.dni = dni;
    }
    public void setDomicilio(String domicilio)
    {
        this.domicilio = domicilio;
    }
    public void setNumeroTelefono(Integer numeroTelefono)
    {
        this.numeroTelefono = numeroTelefono;
    }

    // Metodos getter
    public String getNombre()
    {
        return this.nombre;
    }
    public String getApellido()
    {
        return this.apellido;
    }
    public Integer getDNI()
    {
        return this.dni;
    }
    public String getDomicilio()
    {
        return this.domicilio;
    }
    public Integer getNumeroTelefono()
    {
        return this.numeroTelefono;
    }

}