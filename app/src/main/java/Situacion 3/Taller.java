package situacion;
import java.util.ArrayList;

import javax.accessibility.AccessibleText;


public class Taller
{
    // Metodos
    /*
        - Perdida de aceite - Razon: Junta de tapa - Cambiar junta de tapa - Coste mano de obra 2500$
        - Revision de cubiertas - Alineado, balancedo, cambio de cubiertas delanteras - Respuesto: 2 cubiertas - Coste de mano obra: $22000
        - Chirrido al frenar - Cambio de pastillas de freno - Respuesto: Pastillas de freno - Coste mano de obra: $3000
    */
    

    public static long especificarCostes(Cliente cliente)
    {
        long costo=0;
        for (String i : cliente.vehiculo.especificarMotivos())
        {
            if (i == "Perdida de aceite")
            {
                costo += 2500;
            }
            else if (i == "Revision de cubierta")
            {
                costo += 22000;
            }
            else if (i == "Chirrido al frenar")
            {
                costo += 3000;
            }
        }
        return costo;
    }
    public static ArrayList<String> indicarReparaciones(Cliente cliente)
    {
        ArrayList<String> reparaciones = new ArrayList<String>();
        for (String i : cliente.vehiculo.especificarMotivos())
        {
            if (i == "Perdida de aceite")
            {
                reparaciones.add("- El problema era la junta de tapa. Fue reemplazada");
            }
            else if (i == "Revision de cubierta")
            {
                reparaciones.add ("- Control de estado de cubiertas: Alineado, balancedo y cambio de cubiertas delanteras");
            }
            else
            {
                reparaciones.add ("- Cambio de pastillas de freno");
            }
        }

        return reparaciones;
    }
    public void efectuarFicha(Cliente cliente)
    {
        System.out.println("Datos del dueño");
        System.out.println("Nombre: " + cliente.getNombre() + " " + cliente.getApellido());
        System.out.println("DNI: " + cliente.getDNI());
        System.out.println("Contacto: " + cliente.getNumeroTelefono());
        System.out.println("Domicilio: " + cliente.getDomicilio());

        System.out.println("\nDatos del vehiculo");
        System.out.println("Marca y modelo: " + cliente.vehiculo.getMarca() + " " + cliente.vehiculo.getModelo());
        System.out.println("Patente: " + cliente.vehiculo.getPatente());
        System.out.println("Kilometraje: " + cliente.vehiculo.getKilometraje());

        System.out.println("\nDatos de reparacion: ");
        for (String i : Taller.indicarReparaciones(cliente))
        {
            System.out.println("  " + i);
        }
        System.out.println("Coste: " + especificarCostes(cliente) + " pesos");
    }
    
}